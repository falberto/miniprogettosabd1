package conf;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import query3.Query3Optimized;
import query3.Query3Optimized.AverageReducer;
import query3.Query3Optimized.ClassifierReducerLastYear;
import query3.Query3Optimized.ClassifierReducerNow;
import query3.Query3Optimized.CountAverageTuple;
import query3.Query3Optimized.FilterReducerLastYear;
import query3.Query3Optimized.FilterReducerNow;
import query3.Query3Optimized.MiddleValueMovieIdSwapMapperLastYear;
import query3.Query3Optimized.MiddleValueMovieIdSwapMapperNow;
import query3.Query3Optimized.RatingsToMoovieMapperLastYear;
import query3.Query3Optimized.RatingsToMoovieMapperNow;

public class ConfQuery3Optimized {
	
	public static final long nowUpperBound = 1427846399;
	public static final long nowLowerBound = 1396310400;
	
	public static final long pos = 10;
	

	public static final long LastYearUpperBound = 1396310399;
	public static final long LastYearLowerBound = 1364774400;
	
	public static enum MATCH_COUNTER {
		  LASTYEAR_POS,
		  NOW_POS
	};
	
	public static Configuration getFilterNowConfiguration(
			Configuration conf, Path input, Path output) throws IOException{
		
		Job job = Job.getInstance(conf, "Query3Optimized-job1");
		job.setJarByClass(Query3Optimized.class);
		
        /* Map function */
        job.setMapperClass(RatingsToMoovieMapperNow.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(CountAverageTuple.class);
        job.setCombinerClass(AverageReducer.class);
        /* Reduce function */
        job.setReducerClass(FilterReducerNow.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(FloatWritable.class);
        job.setNumReduceTasks(1);
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);
        
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        /* Wait for job termination */
        //System.exit(job.waitForCompletion(true) ? 0 : 1);
		return job.getConfiguration();
	}
	
	public static Configuration getClassificNowConfiguration(
			Configuration conf, Path input, Path output) throws IOException{
		
		 /* **** Job #2: Ordering phase **** */
    	Job orderJob = Job.getInstance(conf, "Query3Optimized-job2");
    	orderJob.setJarByClass(Query3Optimized.class);
    	/* Map function */
    	orderJob.setMapperClass(MiddleValueMovieIdSwapMapperNow.class);
    	orderJob.setMapOutputKeyClass(FloatWritable.class);
    	orderJob.setMapOutputValueClass(IntWritable.class);
    	//orderJob.setSortComparatorClass(IntWritable.Comparator.class);
        /* Reduce function */
    	orderJob.setReducerClass(ClassifierReducerNow.class);
    	orderJob.setOutputKeyClass(NullWritable.class);
    	orderJob.setOutputValueClass(Text.class);
    	orderJob.setNumReduceTasks(1);
    	
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(orderJob, input);
        FileOutputFormat.setOutputPath(orderJob, output);
        
        orderJob.setInputFormatClass(KeyValueTextInputFormat.class);
        orderJob.setOutputFormatClass(TextOutputFormat.class);
        return orderJob.getConfiguration();
	}
	
	public static Configuration getFilterLastYearConfiguration(
			Configuration conf, Path input, Path output) throws IOException{
		Job jobLastYear = Job.getInstance(conf, "Query3Optimized-job3");
		jobLastYear.setJarByClass(Query3Optimized.class);
		
		/* Map function */
		jobLastYear.setMapperClass(RatingsToMoovieMapperLastYear.class);
		jobLastYear.setMapOutputKeyClass(IntWritable.class);
		jobLastYear.setMapOutputValueClass(CountAverageTuple.class);
		jobLastYear.setCombinerClass(AverageReducer.class);
        /* Reduce function */
		jobLastYear.setReducerClass(FilterReducerLastYear.class);
		jobLastYear.setOutputKeyClass(IntWritable.class);
        jobLastYear.setOutputValueClass(FloatWritable.class);
        jobLastYear.setNumReduceTasks(1);
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(jobLastYear, input);
        FileOutputFormat.setOutputPath(jobLastYear, output);
        
        jobLastYear.setInputFormatClass(TextInputFormat.class);
        jobLastYear.setOutputFormatClass(TextOutputFormat.class);
        
        return jobLastYear.getConfiguration();
	}
	
	public static Configuration getClassificLastYearConfiguration(
			Configuration conf, Path input, Path output, Path pattern) throws IOException{
		
		Job orderjobLastYear = Job.getInstance(conf, "Query3Optimized-job4");
    	orderjobLastYear.setJarByClass(Query3Optimized.class);
    	orderjobLastYear.addCacheFile(pattern.toUri());
    	/* Map function */
    	orderjobLastYear.setMapperClass(MiddleValueMovieIdSwapMapperLastYear.class);
    	orderjobLastYear.setMapOutputKeyClass(FloatWritable.class);
    	orderjobLastYear.setMapOutputValueClass(IntWritable.class);
        
        /* Reduce function */
    	orderjobLastYear.setReducerClass(ClassifierReducerLastYear.class);
    	orderjobLastYear.setOutputKeyClass(NullWritable.class);
    	orderjobLastYear.setOutputValueClass(Text.class);
    	orderjobLastYear.setNumReduceTasks(1);
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(orderjobLastYear, input);
        FileOutputFormat.setOutputPath(orderjobLastYear, output);
        
        orderjobLastYear.setInputFormatClass(KeyValueTextInputFormat.class);
        orderjobLastYear.setOutputFormatClass(TextOutputFormat.class);
        return orderjobLastYear.getConfiguration();
	}
}
