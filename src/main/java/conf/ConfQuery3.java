package conf;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import query3.Query3;
import query3.Query3.AverageReducer;
import query3.Query3.ClassifierReducerLastYear;
import query3.Query3.ClassifierReducerNow;
import query3.Query3.CountAverageTuple;
import query3.Query3.DiffClassificPositionMapper;
import query3.Query3.DiffClassificReducer;
import query3.Query3.FilterReducerLastYear;
import query3.Query3.FilterReducerNow;
import query3.Query3.MiddleValueMovieIdSwapMapperLastYear;
import query3.Query3.MiddleValueMovieIdSwapMapperNow;
import query3.Query3.RatingsToMoovieMapperLastYear;
import query3.Query3.RatingsToMoovieMapperNow;


public class ConfQuery3 {


	public static final long nowUpperBound = 1427846399;
	public static final long nowLowerBound = 1396310400;
	
	public static final long pos = 10;
	

	public static final long LastYearUpperBound = 1396310399;
	public static final long LastYearLowerBound = 1364774400;
	
	public static enum MATCH_COUNTER {
		  LASTYEAR_POS,
		  NOW_POS
	};
	
	public static Configuration getFilterNowConfiguration(
			Configuration conf, Path input, Path output) throws IOException{
		
		Job job = Job.getInstance(conf, "Query3-job1");
		job.setJarByClass(Query3.class);
		
        /* Map function */
        job.setMapperClass(RatingsToMoovieMapperNow.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(CountAverageTuple.class);
        job.setCombinerClass(AverageReducer.class);
        /* Reduce function */
        job.setReducerClass(FilterReducerNow.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(FloatWritable.class);
        job.setNumReduceTasks(1);
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);
        
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        /* Wait for job termination */
        //System.exit(job.waitForCompletion(true) ? 0 : 1);
		return job.getConfiguration();
	}
	
	public static Configuration getClassificNowConfiguration(
			Configuration conf, Path input, Path output) throws IOException{
		
		 /* **** Job #2: Ordering phase **** */
    	Job orderJob = Job.getInstance(conf, "Query3-job2");
    	orderJob.setJarByClass(Query3.class);
    	/* Map function */
    	orderJob.setMapperClass(MiddleValueMovieIdSwapMapperNow.class);
    	orderJob.setMapOutputKeyClass(FloatWritable.class);
    	orderJob.setMapOutputValueClass(Text.class);
        
        /* Reduce function */
    	orderJob.setReducerClass(ClassifierReducerNow.class);
    	orderJob.setOutputKeyClass(FloatWritable.class);
    	orderJob.setOutputValueClass(Text.class);
    	orderJob.setNumReduceTasks(1);
    	
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(orderJob, input);
        FileOutputFormat.setOutputPath(orderJob, output);
        
        orderJob.setInputFormatClass(TextInputFormat.class);
        orderJob.setOutputFormatClass(TextOutputFormat.class);
        return orderJob.getConfiguration();
	}
	
	public static Configuration getFilterLastYearConfiguration(
			Configuration conf, Path input, Path output) throws IOException{
		Job jobLastYear = Job.getInstance(conf, "Query3-job3");
		jobLastYear.setJarByClass(Query3.class);
		
		/* Map function */
		jobLastYear.setMapperClass(RatingsToMoovieMapperLastYear.class);
		jobLastYear.setMapOutputKeyClass(IntWritable.class);
		jobLastYear.setMapOutputValueClass(CountAverageTuple.class);
		jobLastYear.setCombinerClass(AverageReducer.class);
        /* Reduce function */
		jobLastYear.setReducerClass(FilterReducerLastYear.class);
		jobLastYear.setOutputKeyClass(IntWritable.class);
        jobLastYear.setOutputValueClass(FloatWritable.class);
        jobLastYear.setNumReduceTasks(1);
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(jobLastYear, input);
        FileOutputFormat.setOutputPath(jobLastYear, output);
        
        jobLastYear.setInputFormatClass(TextInputFormat.class);
        jobLastYear.setOutputFormatClass(TextOutputFormat.class);
        
        return jobLastYear.getConfiguration();
	}
	
	public static Configuration getClassificLastYearConfiguration(
			Configuration conf, Path input, Path output) throws IOException{
		
		Job orderjobLastYear = Job.getInstance(conf, "Query3-job4");
    	orderjobLastYear.setJarByClass(Query3.class);
    	/* Map function */
    	orderjobLastYear.setMapperClass(MiddleValueMovieIdSwapMapperLastYear.class);
    	orderjobLastYear.setMapOutputKeyClass(FloatWritable.class);
    	orderjobLastYear.setMapOutputValueClass(IntWritable.class);
        
        /* Reduce function */
    	orderjobLastYear.setReducerClass(ClassifierReducerLastYear.class);
    	orderjobLastYear.setOutputKeyClass(FloatWritable.class);
    	orderjobLastYear.setOutputValueClass(Text.class);
    	orderjobLastYear.setNumReduceTasks(1);
        /* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(orderjobLastYear, input);
        FileOutputFormat.setOutputPath(orderjobLastYear, output);
        
        orderjobLastYear.setInputFormatClass(TextInputFormat.class);
        orderjobLastYear.setOutputFormatClass(TextOutputFormat.class);
        return orderjobLastYear.getConfiguration();
	}
	
	public static Configuration getDiffPosLastYearNowConfiguration(
			Configuration conf, Path input, Path output, Path pattern) throws IOException{
		
		//qui eseguo l'ultimo JobMapReduce
    	Job jobDiffPosNowLastYear = Job.getInstance(conf, "Query3-job5");
    	jobDiffPosNowLastYear.setJarByClass(Query3.class);
    	jobDiffPosNowLastYear.addCacheFile(pattern.toUri());
    	 /* Map function */
    	jobDiffPosNowLastYear.setMapperClass(DiffClassificPositionMapper.class);
    	jobDiffPosNowLastYear.setMapOutputKeyClass(IntWritable.class);
    	jobDiffPosNowLastYear.setMapOutputValueClass(Text.class);
    	
    	/* Reduce function */
    	jobDiffPosNowLastYear.setReducerClass(DiffClassificReducer.class);
    	jobDiffPosNowLastYear.setOutputKeyClass(NullWritable.class);
    	jobDiffPosNowLastYear.setOutputValueClass(Text.class);
    	jobDiffPosNowLastYear.setNumReduceTasks(1);
    	/* Set input and output files/directories using command line arguments */
        FileInputFormat.addInputPath(jobDiffPosNowLastYear, input);
        FileOutputFormat.setOutputPath(jobDiffPosNowLastYear, output);
        
        jobDiffPosNowLastYear.setInputFormatClass(TextInputFormat.class);
        jobDiffPosNowLastYear.setOutputFormatClass(TextOutputFormat.class);
		return jobDiffPosNowLastYear.getConfiguration();
	}
}
