package query3;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.LinkedList;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.jobcontrol.JobControl;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.jobcontrol.ControlledJob;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.StringUtils;

import conf.ConfQuery3Optimized;
import conf.ConfQuery3Optimized.MATCH_COUNTER;

/**
 * Questa classe contiene una definizione più ottimizzata del job di MapReduce che risponde alla query3
 * proposta durante il corso di SABD per l'anno accademico 2016/2017 come terzo quesito
 * del primo miniprogetto di metà corso. 
 * 
 * Si riporta il testo della query3:
 * 
 *  	3. Trovare i 10 film che hanno ottenuto la piu alta valutazione 
 *  		nell’ultimo anno del dataset (dal 1 Aprile `
 *  		2014 al 31 Marzo 2015) e confrontare, laddove possibile, 
 *  		la loro posizione nella classifica rispetto a
 * 			quella conseguita nell’anno precedente 
 * 			(dal 1 Aprile 2013 al 31 Marzo 2014).
 * 
 * Si è scelto di utilizzare in questa soluzione proposta 4 Job MapReduce 
 * per risolvere tale query, così indicato:
 * 	
 * 		job1  |  
 * 			  | -> job3	-> job4 
 *		job2  |		   
 *
 * quindi si sono parallelizzati due jobs, utilizzando la classe ControlJob,
 * e definendo tali job come ControlledJob, e indicando le relative dipendenze.
 * 
 * Tra la fase di map del job1 e quella di reduce e tra la fse di map del 
 * job2 e quella di reduce si è inserito un elemento di combiner, 
 * gestendo il fatto che la media dei ratings degli utenti 
 * non è una operazione associativa.
 * 
 * @author falberto
 *
 */

public class Query3Optimized{

	/**
	 * Per la gestione del conteggio della media nel combiner e nel reducer,
	 * poiché la media non gode della proprietà associativa, viene definita
	 * una struttura dati ad hoc che estende Writable in modo da poterla emettere
	 * come value nella fase di map, processarla nel combiner, per poi passarla alla
	 * fase di reducer pre-processata
	 * 
	 * @author falberto
	 *
	 */
	
	public static class CountAverageTuple implements Writable {
		private float count;
		private float average;
		
		public CountAverageTuple() {
			super();
		}
		
		public float getCount() {
			return count;
		}

		public float getAverage() {
			return average;
		}
		public void setAverage(float average) {
			this.average = average;
		}
		public void setCount(float count) {
			this.count = count;
		}
		public void write(DataOutput out) throws IOException {
			// TODO Auto-generated method stub
			out.writeFloat(count);
			out.writeFloat(average);
		}
		public void readFields(DataInput in) throws IOException {
			// TODO Auto-generated method stub
			count = in.readFloat();
			average = in.readFloat();
		}
		
	}
	
	/**
	 * Questa classe costituisce il combiner del job MapReduce, che tiene conto
	 * del numero di value che tra fase di map e fase di reduce vengono pre-sommati
	 * così da far arrivare alla fase di reduce un numero inferiore di values
	 * da dover sottoporre alla fase di reduce. In questo modo viene gestita 
	 * la non associatività dell'operazione di media tenendo appunto conto in "count" 
	 * del numero di value sommati in questa fase. 
	 * 
	 * @author falberto
	 *
	 */
	public static class AverageReducer extends
	Reducer<IntWritable, CountAverageTuple, IntWritable, CountAverageTuple> {
	private CountAverageTuple result = new CountAverageTuple();
	public void reduce(IntWritable key, 
			Iterable<CountAverageTuple> values, Context context) throws IOException, InterruptedException {
		float sum = 0;
		float count = 0;
		// Iterate through all input values for this key
		for (CountAverageTuple val : values) {
			sum += val.getAverage();
			count += val.getCount();
		}
		result.setCount(count);
		result.setAverage(sum);
		context.write(key, result);
		}
	}
	
	/**
	 * Questa classe rappresenta la fase di Map del Job1. Arriva 
	 * l'intero file di ratings.csv, il quale viene splittato, 
	 * e vengono scartati i voti degli utenti con data compresa tra quelle indicate
	 * per la query3. Sfruttando la classe CountAverageTuple,
	 * viene emessa una tupla con chiave movieId e con value un oggetto di tipo 
	 * CountAverageTuple, che ha come "count" 1, e come "average" il voto del singolo
	 * utente che rispetta la data temporale indicata nella query3.
	 * 
	 * @author falberto
	 *
	 */
	public static class RatingsToMoovieMapperNow
    extends Mapper<Object, Text, IntWritable, CountAverageTuple> {
		
		private Text userId = new Text();
		private Text movieId = new Text();
		private float rating;
		private long date;
		
		private IntWritable outKey= new IntWritable();
		private CountAverageTuple outCountAverage = new CountAverageTuple();
		public void map(Object key, Text value, Context context) 
				throws IOException, InterruptedException {	
			String line = value.toString();
            
            line = line.replaceAll(",", " ");
            
            StringTokenizer itr = new StringTokenizer(line);
            userId.set(itr.nextToken());
            if(!(userId.toString()).equals("userId")){
	            movieId.set(itr.nextToken());
	            rating = Float.parseFloat(itr.nextToken());
	            date = Long.parseLong(itr.nextToken());
	            if(date >= ConfQuery3Optimized.nowLowerBound 
	            		&& date <= ConfQuery3Optimized.nowUpperBound){
	            	outKey.set(Integer.parseInt(movieId.toString()));
			    	outCountAverage.setCount(1);
	            	outCountAverage.setAverage(rating);
			    	context.write(outKey, outCountAverage);		    	
	            }
			}
            else{
            	itr.nextToken();
            	itr.nextToken();
            	itr.nextToken();
            }
		}
	}
	/**
	 * Questa è la classe che definisce la fase di Reduce del job1.
	 * In questo caso si eseguono analoghe operazioni viste anche per il combiner.
	 * A differenza del combiner, qui viene emessa la tupla con chiave "key", 
	 * ovvero il MovieId del film, e con value "result", che è di tipo FloatWritable
	 * di valore "sum/count"
	 * 
	 * @author falberto
	 *
	 */
	public static class FilterReducerNow
    extends Reducer<IntWritable, CountAverageTuple, IntWritable, FloatWritable> {
		
		private FloatWritable result = new FloatWritable();
		
	public void reduce(IntWritable key, Iterable<CountAverageTuple> values,
	                   Context context
		) throws IOException, InterruptedException {
			float sum = 0;
			float count = 0;
			// Iterate through all input values for this key
			for (CountAverageTuple val : values) {
				sum += val.getAverage();
				count += val.getCount();
			}	    
			result.set(sum/count);
	    	context.write(key, result);
		}
	}
	
	/**
	 * Questa classe rappresenta la fase di Map del Job3. Arriva 
	 * il file temporaneo generato come output della fase del job1, 
	 * il quale viene splittato, e viene emessa una tupla con chiave 
	 * middleRating, che però per sfruttare il sorting della fase di 
	 * shuffle and sort, viene moltiplicato per "-1" e con value movieId.
	 * 
	 * @author falberto
	 *
	 */
	public static class MiddleValueMovieIdSwapMapperNow
    extends Mapper<Object, Text, FloatWritable, IntWritable> {
		
		private IntWritable movieId = new IntWritable();
		private FloatWritable middleRating = new FloatWritable();
		
		public void map(Object key, Text value, Context context) 
				throws IOException, InterruptedException {	
			
            movieId.set(Integer.parseInt(key.toString()));
            middleRating.set(-1*Float.parseFloat(value.toString()));
            context.write(middleRating, movieId);
		}
	}
	
	/**
	 * Questa classe rappresenta la fase di Reduce del Job3. Un contatore globale
	 * viene utilizzato dall'unico Reducer per contare nell'ordine decrescente
	 * le tuple che sono arrivate dalla fase di Map, così da emettere come 
	 * output la classifica dei films, considerando films con stesso rating come
	 * ex-equo, e quindi associando a tali films lo stesso valore in classifica.
	 * Poiché vanno considerati solo i primi N elementi della classifica, allora
	 * non vengono emessi nel file di output tutti gli altri.
	 * 
	 * @author falberto
	 *
	 */
	public static class ClassifierReducerNow
    extends Reducer<FloatWritable, IntWritable, NullWritable, Text> {
		private Text result = new Text();
	
		@Override
	    public void run(Context context) throws IOException, InterruptedException {
	    	  setup(context);
	    	  
	    	  while (context.nextKey() && context.getCounter(MATCH_COUNTER.NOW_POS).getValue()+1
	    			  <= ConfQuery3Optimized.pos) {
	    	    reduce(context.getCurrentKey(), context.getValues(), context);
	    	  }
	    	  cleanup(context);
	    	}
	public void reduce(FloatWritable key, Iterable<IntWritable> values,
	                   Context context
		) throws IOException, InterruptedException {
		    for (IntWritable val : values) {
		    	result.set(Long.toString(context.getCounter(
		    			MATCH_COUNTER.NOW_POS).getValue()+1)+"\t"+
		    			Integer.toString(val.get()));
		    	
		    	//if(context.getCounter(MATCH_COUNTER.NOW_POS).getValue()+1<=
		    		//		ConfQuery3Optimized.pos){
		    		context.write(NullWritable.get(), result);
		    	//}	
		    }
		    context.getCounter(MATCH_COUNTER.NOW_POS).increment(1);
		    
		}
	}
	
	/**
	 * Questa classe rappresenta la fase di Map del Job2. Arriva 
	 * l'intero file di ratings.csv, il quale viene splittato, 
	 * e vengono scartati i voti degli utenti con data compresa tra quelle indicate
	 * per la query3. Sfruttando la classe CountAverageTuple,
	 * viene emessa una tupla con chiave movieId e con value un oggetto di tipo 
	 * CountAverageTuple, che ha come "count" 1, e come "average" il voto del singolo
	 * utente che rispetta la data temporale indicata nella query3.
	 * 
	 * @author falberto
	 *
	 */
	
	public static class RatingsToMoovieMapperLastYear
    extends Mapper<Object, Text, IntWritable, CountAverageTuple> {
		
		private Text userId = new Text();
		private Text movieId = new Text();
		private float rating;
		private long date;
		private IntWritable outKey = new IntWritable();
		private CountAverageTuple outCountAverage = new CountAverageTuple();
		public void map(Object key, Text value, Context context) 
				throws IOException, InterruptedException {	
			String line = value.toString();
            
            line = line.replaceAll(",", " ");
            
            StringTokenizer itr = new StringTokenizer(line);
            userId.set(itr.nextToken());
            if(!(userId.toString()).equals("userId")){
	            movieId.set(itr.nextToken());
	            rating = Float.parseFloat(itr.nextToken());;
	            date = Long.parseLong(itr.nextToken());
	            if(date >= ConfQuery3Optimized.LastYearLowerBound 
	            		&& date <= ConfQuery3Optimized.LastYearUpperBound){
	            	outKey.set(Integer.parseInt(movieId.toString()));
	            	outCountAverage.setCount(1);
	            	outCountAverage.setAverage(rating);
			    	context.write(outKey, outCountAverage);	
	            }
			}
            else{
            	itr.nextToken();
            	itr.nextToken();
            	itr.nextToken();
            }
		}
	}
	
	/**
	 * Questa è la classe che definisce la fase di Reduce del job2.
	 * In questo caso si eseguono analoghe operazioni viste anche per il combiner.
	 * A differenza del combiner, qui viene emessa la tupla con chiave "key", 
	 * ovvero il MovieId del film, e con value "result", che è di tipo FloatWritable
	 * di valore "sum/count"
	 * 
	 * @author falberto
	 *
	 */
	
	public static class FilterReducerLastYear
    extends Reducer<IntWritable, CountAverageTuple, IntWritable, FloatWritable> {
	
	private FloatWritable result = new FloatWritable();
	public void reduce(IntWritable key, Iterable<CountAverageTuple> values,
	                   Context context
		) throws IOException, InterruptedException {
		    
	    	float sum = 0;
			float count = 0;
			// Iterate through all input values for this key
			for (CountAverageTuple val : values) {
				sum += val.getAverage();
				count += val.getCount();
			}	    
			result.set(sum/count);
	    	context.write(key, result);
		}
	}
	
	/**
	 * Questa classe rappresenta la fase di Map del Job4. Arriva 
	 * il file temporaneo generato come output della fase del job2, 
	 * il quale viene splittato, e viene emessa una tupla con chiave 
	 * middleRating, che però per sfruttare il sorting della fase di 
	 * shuffle and sort, viene moltiplicato per "-1" e con value movieId.
	 * 
	 * @author falberto
	 *
	 */
	public static class MiddleValueMovieIdSwapMapperLastYear
    extends Mapper<Object, Text, FloatWritable, IntWritable> {
		
		private Text movieId = new Text();
		private FloatWritable middleRating = new FloatWritable();
		
		public void map(Object key, Text value, Context context) 
				throws IOException, InterruptedException {	
            movieId.set(key.toString());
            middleRating.set(-1*Float.parseFloat(value.toString()));
            context.write(middleRating, new IntWritable(Integer.parseInt(
            		movieId.toString())));
		}
	}
	
	/**
	 * Questa classe rappresenta la fase di Reduce del Job4. Un contatore globale
	 * viene utilizzato dall'unico Reducer per contare nell'ordine decrescente
	 * le tuple che sono arrivate dalla fase di Map. In questa versione il reducer
	 * ha una fase di setup che carica il file dell'output precedente, e parsandolo
	 * vengono riempite due liste. Se il film corrente nella fase di reduce
	 * è nella lista dei film caricati allora viene emesso come output della fase,
	 * registando id del film, posizione dell'anno attuale e quella dell'anno precedente,
	 * altrimenti viene saltato. In questo modo si minimizza l'interazione con l'I/O, 
	 * si riduce in numero di Job impiegati, e si salva direttamente il file su HDFS, 
	 * senza doverlo ricaricare e riprocessare in un altro Job MapReduce.
	 * 
	 * @author falberto
	 *
	 */
	
	public static class ClassifierReducerLastYear
    extends Reducer<FloatWritable, IntWritable, NullWritable, Text> {
		private Text result = new Text();
		private Configuration conf;
	    private BufferedReader fis;
	    private LinkedList<String> posList = new LinkedList<String>();
	    private LinkedList<String> movieIdList = new LinkedList<String>();
		@Override
        public void setup(Context context) throws IOException,
                InterruptedException {
    	 	conf = context.getConfiguration();
    	 	URI[] patternsURIs = Job.getInstance(conf).getCacheFiles();
            for (URI patternsURI : patternsURIs) {
                Path patternsPath = new Path(patternsURI.getPath());
                String patternsFileName = patternsPath.getName().toString();
                parseSkipFile(patternsFileName);
            }
		}
	     private void parseSkipFile(String fileName) {      
		     try {
	             fis = new BufferedReader(new FileReader(fileName));
	             String pattern = null;
	             while ((pattern = fis.readLine()) != null) {
	             	StringTokenizer itr = new StringTokenizer(
	             			pattern);
	             	posList.add(itr.nextToken());
	             	movieIdList.add(itr.nextToken());
	
	             }
	         } catch (IOException ioe) {
	             System.err.println("Caught exception while parsing the cached file '"
	                     + StringUtils.stringifyException(ioe));
	         }
	     }
	    
	    @Override
		public void reduce(FloatWritable key, Iterable<IntWritable> values,
		                   Context context
			) throws IOException, InterruptedException {
			
			    for (IntWritable val : values) {
			    	result.set(Long.toString(
			    			context.getCounter(MATCH_COUNTER.LASTYEAR_POS).getValue()+1)
			    			+"\t"+ Integer.toString(val.get()));
			    	int i = 0;
			    	for(String mId : movieIdList){
			        	 if(mId.equals(Integer.toString(val.get()))){  
			        		 result.set(result.toString()+"\t"+
			        				 Long.parseLong(posList.get(i)));
			        		 context.write(NullWritable.get(), result);
			        		 break;
			        	 }
			        	 ++i;
			    	}
			    }	 
			    context.getCounter(MATCH_COUNTER.LASTYEAR_POS).increment(1);
			}
		}

	public static void main(String[] args) throws Exception {
		
		/* Create and configure a new MapReduce Job */
		Configuration conf = new Configuration();
		
		/* *** Parse the arguments passed through the command line *** */
        GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        String[] remainingArgs = optionParser.getRemainingArgs();
        Path inputFile = null;
        Path outputFileNow = null;
        Path outputFileLastYear = null;
        Path outputStageNow= null;
        Path outputStageLastYear= null;
    	Path patternClassificFile = null;
    	
        if (remainingArgs.length != 2) {
            System.err.println("Usage: DistributedGrep<in> <out>");
            System.exit(2);
        } else {
            inputFile = new Path(remainingArgs[0]);
            outputFileNow = new Path(remainingArgs[1]+"Now");
            outputStageNow = new Path(outputFileNow + "_stagingNow");
            outputFileLastYear = new Path(remainingArgs[1]+"DiffPosNowLastYear");
            outputStageLastYear = new Path(outputFileNow + "_stagingLastYear");
            patternClassificFile = new Path(remainingArgs[1]+"Now/part-r-00000");
        }
        
		ControlledJob filterNow = new ControlledJob(ConfQuery3Optimized.getFilterNowConfiguration(
				conf, inputFile, outputStageNow));
		ControlledJob classificNow = new ControlledJob(ConfQuery3Optimized.getClassificNowConfiguration(
				conf, outputStageNow, outputFileNow));
		ControlledJob filterLastYear = new ControlledJob(ConfQuery3Optimized.getFilterLastYearConfiguration(
				conf, inputFile, outputStageLastYear));
		ControlledJob classificLastYear = new ControlledJob(ConfQuery3Optimized.getClassificLastYearConfiguration(
				conf, outputStageLastYear, outputFileLastYear,patternClassificFile));
		
		classificNow.addDependingJob(filterNow);
		classificLastYear.addDependingJob(filterLastYear);
		classificLastYear.addDependingJob(classificNow);
		
		int code = 1;
		JobControl jc = new JobControl("Query3JobControl");
		
		jc.addJob(filterNow);
		jc.addJob(classificNow);
		jc.addJob(filterLastYear);
		jc.addJob(classificLastYear);
		Thread jobControlThread = new Thread(jc);
	    jobControlThread.start();
	    while (!jc.allFinished()) {
	        System.out.println("Jobs in waiting state: " + jc.getWaitingJobList().size());  
	        System.out.println("Jobs in ready state: " + jc.getReadyJobsList().size());
	        System.out.println("Jobs in running state: " + jc.getRunningJobList().size());
	        System.out.println("Jobs in success state: " + jc.getSuccessfulJobList().size());
	        System.out.println("Jobs in failed state: " + jc.getFailedJobList().size());
		    try {
		        Thread.sleep(1000);
		        } catch (Exception e) {
	
		        }
	    }
		code = jc.getFailedJobList().size() == 0 ? 0 : 1;
		 
        FileSystem.get(new Configuration()).delete(outputStageNow, true);
        FileSystem.get(new Configuration()).delete(outputStageLastYear, true);
        /* Wait for job termination */
        System.exit(code);
          
        
	}
}


