package query1;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import conf.ConfQuery1;

/**
 * Questa classe contiene la definizione del job di MapReduce che risponde alla query1
 * proposta durante il corso di SABD per l'anno accademico 2016/2017 come primo quesito
 * del primo miniprogetto di metà corso.
 * 
 * Si riporta il testo della query1:
 * 
 *  	"1. Individuare i film con una 
 *  		valutazione maggiore o uguale a 4.0 e 
 *  		valutati a partire dal 1 Gennaio 2000."
 * 
 * Si è scelto di utilizzare un unico Job MapReduce per risolvere tale query, 
 * e si è inserito un elemento di combiner, gestendo il fatto che la media
 * dei ratings degli utenti non è una operazione associativa.
 * 
 * @author falberto
 *
 */

public class Query1 {
	
	/**
	 * Per la gestione del conteggio della media nel combiner e nel reducer,
	 * poiché la media non gode della proprietà associativa, viene definita
	 * una struttura dati ad hoc che estende Writable in modo da poterla emettere
	 * come value nella fase di map, processarla nel combiner, per poi passarla alla
	 * fase di reducer pre-processata
	 * 
	 * @author falberto
	 *
	 */
	
	public static class CountAverageTuple implements Writable {
		private int count;
		private double average;
		
		public CountAverageTuple() {
			super();
		}
		
		public double getCount() {
			return count;
		}

		public double getAverage() {
			return average;
		}
		public void setAverage(double average) {
			this.average = average;
		}
		public void setCount(int count) {
			this.count = count;
		}
		public void write(DataOutput out) throws IOException {
			// TODO Auto-generated method stub
			out.writeInt(count);
			out.writeDouble(average);
		}
		public void readFields(DataInput in) throws IOException {
			// TODO Auto-generated method stub
			count = in.readInt();
			average = in.readDouble();
		}
		
	}
	
	/**
	 * Questa classe costituisce il combiner del job MapReduce, che tiene conto
	 * del numero di value che tra fase di map e fase di reduce vengono pre-sommati
	 * così da far arrivare alla fase di reduce un numero inferiore di values
	 * da dover sottoporre alla fase di reduce. In questo modo viene gestita 
	 * la non associatività dell'operazione di media tenendo appunto conto in "count" 
	 * del numero di value sommati in questa fase. 
	 * 
	 * @author falberto
	 *
	 */
	public static class AverageReducer extends
		Reducer<IntWritable, CountAverageTuple, IntWritable, CountAverageTuple> {
		private CountAverageTuple result = new CountAverageTuple();
		public void reduce(IntWritable key, 
				Iterable<CountAverageTuple> values, Context context) throws IOException, InterruptedException {
			double sum = 0.0;
			int count = 0;
			// Iterate through all input values for this key
			for (CountAverageTuple val : values) {
				sum += val.getAverage();
				count += val.getCount();
			}
			result.setCount(count);
			result.setAverage(sum);
			context.write(key, result);
			}
	}
	
	/**
	 * Questa classe rappresenta la fase di Map. Arriva l'intero file di ratings.csv,
	 * il quale viene splittato, e vengono scartati i voti degli utenti con data 
	 * superiore a quella della query1. Sfruttando la classe CountAverageTuple,
	 * viene emessa una tupla con chiave movieId e con value un oggetto di tipo 
	 * CountAverageTuple, che ha come "count" 1, e come "average" il voto del singolo
	 * utente che rispetta la data temporale indicata nella query1.
	 * 
	 * @author falberto
	 *
	 */
	public static class RatingsToMoovieMapper
    extends Mapper<Object, Text, IntWritable, CountAverageTuple> {
		
		private Text userId = new Text();
		private Text movieId = new Text();
		private float rating;
		private long date;
		private IntWritable result = new IntWritable();
		private CountAverageTuple outCountAverage = new CountAverageTuple();

		public void map(Object key, Text value, Context context) 
				throws IOException, InterruptedException {	
			String line = value.toString();
            
            line = line.replaceAll(",", " ");
            
            StringTokenizer itr = new StringTokenizer(line);
            userId.set(itr.nextToken());
            if(!(userId.toString()).equals("userId")){
	            movieId.set(itr.nextToken());

	            rating = Float.parseFloat(itr.nextToken());
	            date = Long.parseLong(itr.nextToken());
	            if(date >= ConfQuery1.date){
	            	outCountAverage.setCount(1);
	            	outCountAverage.setAverage(rating);
	            	result.set(Integer.parseInt(movieId.toString()));
			    	context.write(result, outCountAverage);
	            }
			}
            else{
            	itr.nextToken();
            	itr.nextToken();
            	itr.nextToken();

            }
		}
	}
	
	/**
	 * Questa è la classe che definisce la fase di Reduce dell'applicazione.
	 * In questo caso si eseguono analoghe operazioni viste anche per il combiner.
	 * A differenza del combiner, qui c'è un filtro con una soglia specificata 
	 * dai requisiti della query1 e relativi al valore di rating medio da considerare.
	 * Se tale valore è rispettato, allora viene emessa la tupla con chiave "key", 
	 * ovvero il MovieId del film, e con value "result", che è di tipo DoubleWritable
	 * di valore "sum/count"
	 * 
	 * @author falberto
	 *
	 */
	
	public static class FilterAverageReducer
	    extends Reducer<IntWritable, CountAverageTuple, IntWritable, DoubleWritable> {
		private DoubleWritable result = new DoubleWritable();
		public void reduce(IntWritable key, 
				Iterable<CountAverageTuple> values, Context context
		) throws IOException, InterruptedException {
			float sum = 0;
			float count = 0;
			// Iterate through all input values for this key
			for (CountAverageTuple val : values) {
				sum += val.getAverage();
				count += val.getCount();
			}   
		    if(sum/count >= ConfQuery1.rating){
		    	result.set(sum/count);
		    	context.write(key, result);
		    }
			

		}
	}
	
	public static void main(String[] args) throws Exception {
		
		    

		/* Viene creato e configurato un nuovo job MapReduce di nome "Query1" */
		
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "Query1");
		job.setJarByClass(Query1.class);
		

		/* Vengono parsati gli argomenti del programma */

        GenericOptionsParser optionParser = new GenericOptionsParser(conf, args);
        String[] remainingArgs = optionParser.getRemainingArgs();
        Path inputFile = null;
        Path outputFile = null;

        int N = 1;
        /* Viene specificato un controllo sull'errore 
         * per gli argomenti necessari al programma*/
        

        if (remainingArgs.length != 3) {
            System.err.println("Usage: query1.Query1 <in> <out> <#Reducers>");
            System.exit(2);
        } else {
            inputFile = new Path(remainingArgs[0]);
            outputFile = new Path(remainingArgs[1]);
            try{
            	N = Integer.parseInt(remainingArgs[2]);
            }
            catch(Exception e){
            	N = 1;
            }
            	
        }
        
        /* La funzione di Map*/
        
        job.setMapperClass(RatingsToMoovieMapper.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(CountAverageTuple.class);

        job.setCombinerClass(AverageReducer.class);
        
        /* La funzione di Reduce */
        
        job.setReducerClass(FilterAverageReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(DoubleWritable.class);
        /* Si sceglie di settare il numero di reducers a 1 */
        job.setNumReduceTasks(N);
        
		/* Setto i files/directory di input e output usando gli argomenti presi dalla
		 * command line */
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        
        /* Si aspetta la terminazione del job */
        

        System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}